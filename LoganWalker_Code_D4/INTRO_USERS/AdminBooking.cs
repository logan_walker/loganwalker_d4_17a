﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;

namespace INTRO_USERS
{
    public partial class AdminBooking : Form
    {

        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS;Integrated Security=True");
        //SqlConnection con = new SqlConnection(@"Data Source=TYLER-PC\SQLEXPRESS;Database=INTRO_USERS;Integrated Security=True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader read;

        public string type = "";

        public AdminBooking(string CurrentUser)
        {
            InitializeComponent();

            label9.Text = CurrentUser;

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = INTRO_USERS; Integrated Security = True"))
            {
                SqlCommand sqlCmd1 = new SqlCommand("SELECT Name FROM Client", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader1 = sqlCmd1.ExecuteReader();

                // name = SQL.read[2].ToString() + SQL.read[3].ToString();

                while (sqlReader1.Read())
                {
                    comboBoxClient.Items.Add(sqlReader1["Name"].ToString());
                }
                sqlReader1.Close();


                SqlCommand sqlCmd2 = new SqlCommand("SELECT FirstName, LastName FROM Instructor WHERE FirstName != 'Spare'", sqlConnection);
                SqlDataReader sqlReader2 = sqlCmd2.ExecuteReader();

                // name = SQL.read[2].ToString() + SQL.read[3].ToString();

                while (sqlReader2.Read())
                {
                    comboBoxInstructor.Items.Add(sqlReader2["FirstName"].ToString() + " " + (sqlReader2["LastName"].ToString()));
                }
                sqlReader2.Close();
            }
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            //You should have seen this from the register page, same code to switch forms.
            Hide();
            LoginPage login = new LoginPage();
            login.ShowDialog();
            Close();
        }

        /// <summary>
        /// Gets the social media id based on the social media name
        /// </summary>
        /// <param name="socialMedia">The name of the social media from combo box</param>
        /// <returns>The ID of the social media from database, blank string returned if not in database</returns>


        private void ClientBookTimes_Load(object sender, EventArgs e)
        {
            ResetColor();
        }

        public static string timeslot;
        public static string day;
        public static string instructor;
        public static string client;

        private void button16_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "8.00am-9.00am";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button16.BackColor = Color.DarkGray;
        }

        public void button1_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "7.00am-8.00am";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button1.BackColor = Color.DarkGray;
        }

        public void button8_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "9.00am-10.00am";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button8.BackColor = Color.DarkGray;
        }

        public void button3_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "10.00am-11.00am";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button3.BackColor = Color.DarkGray;
        }

        public void button10_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "11.00am-12.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button10.BackColor = Color.DarkGray;
        }

        public void button6_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "12.00pm-1.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button6.BackColor = Color.DarkGray;
        }

        public void button5_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "1.00pm-2.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button5.BackColor = Color.DarkGray;
        }

        public void button4_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "2.00pm-3.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button4.BackColor = Color.DarkGray;
        }

        public void button11_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "3.00pm-4.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button11.BackColor = Color.DarkGray;
        }

        public void button7_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "4.00pm-5.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button7.BackColor = Color.DarkGray;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "5.00pm-6.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button15.BackColor = Color.DarkGray;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "6.00pm-7.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button14.BackColor = Color.DarkGray;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "7.00pm-8.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button12.BackColor = Color.DarkGray;
        }

        public void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            ResetColor();

            string Date = "", selectDay = "";
            Date = dateTimePicker1.Text.ToString();
            selectDay = Date;

            if (selectDay[0] == 'S' && selectDay[1] == 'u')
            {
                button1.Visible = false;
                button3.Visible = false;
                button6.Visible = false;
                button8.Visible = false;
                button11.Visible = false;
                button10.Visible = false;
                button7.Visible = false;
                button5.Visible = false;
                button4.Visible = false;
                button16.Visible = false;
                button15.Visible = false;
                button14.Visible = false;
                button12.Visible = false;
                button13.Visible = false;
                label2.Visible = false;
                label6.Visible = false;
                label7.Visible = false;
                labelSunday.Visible = true;
            }
            else
            {
                button1.Visible = true;
                button3.Visible = true;
                button6.Visible = true;
                button8.Visible = true;
                button11.Visible = true;
                button10.Visible = true;
                button7.Visible = true;
                button5.Visible = true;
                button4.Visible = true;
                button16.Visible = true;
                button15.Visible = true;
                button14.Visible = true;
                button12.Visible = true;
                button13.Visible = true;
                label2.Visible = true;
                label6.Visible = true;
                label7.Visible = true;
                labelSunday.Visible = false;
            }

        }
        public void ResetColor()
        {
            button1.BackColor = Color.FromKnownColor(KnownColor.Control);
            button3.BackColor = Color.FromKnownColor(KnownColor.Control);
            button6.BackColor = Color.FromKnownColor(KnownColor.Control);
            button8.BackColor = Color.FromKnownColor(KnownColor.Control);
            button11.BackColor = Color.FromKnownColor(KnownColor.Control);
            button10.BackColor = Color.FromKnownColor(KnownColor.Control);
            button7.BackColor = Color.FromKnownColor(KnownColor.Control);
            button5.BackColor = Color.FromKnownColor(KnownColor.Control);
            button4.BackColor = Color.FromKnownColor(KnownColor.Control);
            button13.BackColor = Color.FromKnownColor(KnownColor.Control);
            button16.BackColor = Color.FromKnownColor(KnownColor.Control);
            button15.BackColor = Color.FromKnownColor(KnownColor.Control);
            button14.BackColor = Color.FromKnownColor(KnownColor.Control);
            button12.BackColor = Color.FromKnownColor(KnownColor.Control);
            buttonLogin.BackColor = Color.FromKnownColor(KnownColor.Control);
        }

        public void button13_Click(object sender, EventArgs e) //submit button
        {
            //variables to be used
            string Time = "", Date = "", InstructorName = "", CurrentUser = "", email = "";

            Time = timeslot;
            Date = dateTimePicker1.Text.ToString();
            InstructorName = comboBoxInstructor.Text;
            CurrentUser = comboBoxClient.Text;

            string TimeI = "", DateI = "", InstructorNameI = "", CurrentUserI = ""; // TEST CODE

            TimeI = Time;
            DateI = Date;
            InstructorNameI = InstructorName;
            CurrentUserI = CurrentUser;

            if (label7.Text == "")
            {
                MessageBox.Show("Error, Missing Instructor, Client or Time/Date.");
            }
            else
            { // TEST CODE STARTS ------------------------------------------------------------------------

                using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = INTRO_USERS; Integrated Security = True"))
                {
                    // System.Diagnostics.Debug.WriteLine($"########    SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}' AND Client = '{CurrentUserI}'");
                    SqlCommand sqlCmd3 = new SqlCommand($"SELECT Type FROM Client Where Name = '{CurrentUserI}'", sqlConnection);
                    sqlConnection.Open();
                    SqlDataReader sqlReader3 = sqlCmd3.ExecuteReader();

                    // name = SQL.read[2].ToString() + SQL.read[3].ToString();

                    while (sqlReader3.Read())
                    {
                        type = (sqlReader3["Type"].ToString());
                    }
                    sqlReader3.Close();

                    SqlCommand sqlCmd = new SqlCommand($"SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}'", sqlConnection);
                    
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    // name = SQL.read[2].ToString() + SQL.read[3].ToString();  (Time, Date, InstructorName, Client)

                    while (sqlReader.Read())
                    {
                        if (sqlReader.HasRows)
                        {
                            TimeI = sqlReader.GetString(1);
                            DateI = sqlReader.GetString(2);
                            InstructorNameI = sqlReader.GetString(3);
                            //CurrentUserI = sqlReader.GetString(4);

                            if (TimeI == "" && DateI == "" && InstructorNameI == "" && CurrentUserI == "")
                            {

                                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, Type) VALUES ('" + Time + "', '" + Date + "','" + InstructorName + "', '" + CurrentUser + "', 'Booked', '" + type + "')");
                                //success message for the user to know it worked
                                MessageBox.Show("Time successfully booked - Your booked time is: " + Date + " at " + Time + " With " + InstructorName);

                            }
                            else
                            {
                                MessageBox.Show("Time unavalible. Please Pick Another");
                                return;
                            }
                        }
                    }
                    sqlReader.Close();
                }
                // CheckInput(); TEST CODE ENDS ---------------------------------------------------------------
                try
                {
                    SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, Type) VALUES ('" + Time + "', '" + Date + "','" + InstructorName + "', '" + CurrentUser + "', 'Booked', '" + type + "')");
                    //success message for the user to know it worked
                    MessageBox.Show("Time successfully booked - Your booked time is: " + Date + " at " + Time);

                    //Email send and recieve

                    con.Open();
                    cmd.CommandText = ($"SELECT email from Client where Name = '{CurrentUser}'");
                    cmd.Connection = con;
                    read = cmd.ExecuteReader();
                    while (read.Read())
                    {
                        email = read[0].ToString();
                    }
                    con.Close();
                    
                    SmtpClient SmtpServer = new SmtpClient();
                    SmtpServer.Credentials = new System.Net.NetworkCredential("d4databasetest@gmail.com", "database123");
                    SmtpServer.Port = 587;
                    SmtpServer.Host = "smtp.gmail.com";
                    SmtpServer.EnableSsl = true;
                    var mail = new MailMessage();

                    mail.From = new MailAddress("d4databasetest@gmail.com",
                    "DIA - Driving Instruction Academy", System.Text.Encoding.UTF8);
                    mail.To.Add(email);
                    mail.Subject = $"DIA - Appointment Confirmation";
                    mail.Body = ($"---- DIA Driving Instruction Academy----\r\n\r\nHello {CurrentUser},\r\n\r\nSo you want to learn how to drive like a pro?\r\nYou've come to the right place!\r\n\r\nYour appointment is on {Date} at {Time} with {InstructorName}.\r\n(Please come 10 minutes earlier than your appointment time).\r\n\r\nIf this was not you, please contact DIA Driving Instruction Academy on 0800 DRIVING.\r\n\r\nWe'll see you then!\r\n\r\n\r\nRegards,\r\n\r\nDIA - Driving Instruction Academy");
                    mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    SmtpServer.Send(mail);
                    MessageBox.Show($"Email has been sent to: " + email);
                }
                catch (Exception)
                {
                    MessageBox.Show("Appointment successfully booked. However, Email was unable to be sent as the Email address linked to this account is invalid.");
                    return;
                }
            }
        }

        public void button9_Click(object sender, EventArgs e) // BLOCK OUT FULL DAY
        {
            //variables to be used
            string Time = "", Date = "", InstructorName = "", CurrentUser = "";

            Time = timeslot;
            Date = dateTimePicker1.Text.ToString();
            InstructorName = comboBoxInstructor.Text;
            CurrentUser = comboBoxClient.Text;

            string DateI = "", InstructorNameI = "", CurrentUserI = ""; // TEST CODE

            DateI = Date;
            InstructorNameI = InstructorName;
            CurrentUserI = CurrentUser;


            // TEST CODE STARTS ------------------------------------------------------------------------

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = INTRO_USERS; Integrated Security = True"))
            {
                // System.Diagnostics.Debug.WriteLine($"########    SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}' AND Client = '{CurrentUserI}'");


                SqlCommand sqlCmd = new SqlCommand($"SELECT * FROM Timeslot WHERE Date = '{DateI}' AND InstructorName = '{InstructorNameI}' AND Client = '{CurrentUserI}'", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                // name = SQL.read[2].ToString() + SQL.read[3].ToString();  (Time, Date, InstructorName, Client)

                while (sqlReader.Read())
                {
                    if (sqlReader.HasRows)
                    {
                        DateI = sqlReader.GetString(2);
                        InstructorNameI = sqlReader.GetString(3);
                        CurrentUserI = sqlReader.GetString(4);


                        if (DateI == "" && InstructorNameI == "" && CurrentUserI == "")
                        {

                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "7.00am-8.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "8.00am-9.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "9.00am-10.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "10.00am-11.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "11.00am-12.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "12.00am-1.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "1.00pm-2.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "2.00pm-3.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "3.00pm-4.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "4.00pm-5.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "5.00pm-6.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "6.00pm-7.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "7.00pm-8.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            MessageBox.Show("Successfully blocked out whole day on: " + Date);

                        }
                        else
                        {
                            MessageBox.Show("You already have a time blocked out on this day. Cannot block out full day.");
                            return;
                        }
                    }

                }
                sqlReader.Close();
            }
            // CheckInput(); TEST CODE ENDS ---------------------------------------------------------------
            try
            {
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "7.00am-8.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "8.00am-9.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "9.00am-10.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "10.00am-11.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "11.00am-12.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "12.00am-1.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "1.00pm-2.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "2.00pm-3.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "3.00pm-4.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "4.00pm-5.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "5.00pm-6.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "6.00pm-7.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "7.00pm-8.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                MessageBox.Show("Successfully blocked out whole day on: " + Date);
            }
            catch (Exception)
            {
                MessageBox.Show("Time unavalible.");
                return;
            }
        }

        private void buttonLogin_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
