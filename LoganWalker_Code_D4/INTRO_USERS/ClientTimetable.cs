﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class ClientTimetable : Form
    {

        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS;Integrated Security=True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader read;

        public string user = "";

        public void stuff()
        {
            con.Open();
            cmd.CommandText = "select Date, Time, InstructorName from TIMESLOT";
            cmd.Connection = con;
            read = cmd.ExecuteReader();

            while (read.Read())
            {
                listView1.View = View.Details;

                ListViewItem item = new ListViewItem(read["Date"].ToString());
                item.SubItems.Add(read["Time"].ToString());
                item.SubItems.Add(read["InstructorName"].ToString());

                listView1.Items.Add(item);
            }
            con.Close();
        }

        public ClientTimetable(string currentuser)
        {
            InitializeComponent();

            user = currentuser;
            label9.Text = user;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void ClientTimetable_Load(object sender, EventArgs e)
        {
            stuff();
        }
    }
}
