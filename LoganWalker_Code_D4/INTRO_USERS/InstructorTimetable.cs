﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class InstructorTimetable : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS;Integrated Security=True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader read1;

        public string user = "";

        public void loadtable()
        {
            con.Open();
            cmd.CommandText = ($"select ID, Date, Time, InstructorName, Client, Timeoff, Status, Type from TIMESLOT WHERE InstructorName = '{user}'");
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();

            while (read1.Read())
            {
                listView1.View = View.Details;

                ListViewItem item = new ListViewItem(read1["ID"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["Timeoff"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Type"].ToString());

                listView1.Items.Add(item);
            }
            con.Close();
        }

        public void hidestimeoff()
        {
            listView1.Items.Clear();

            con.Open();
            cmd.CommandText = ($"select ID, Date, Time, InstructorName, Client, Timeoff, Status, Type from TIMESLOT WHERE InstructorName = '{user}' AND Timeoff IS NULL");
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();

            while (read1.Read())
            {
                listView1.View = View.Details;

                ListViewItem item = new ListViewItem(read1["ID"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["Timeoff"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Type"].ToString());

                listView1.Items.Add(item);
            }
            con.Close();
        }

        public InstructorTimetable(string currentuser)
        {
            InitializeComponent();

            user = currentuser;
            label9.Text = user;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void InstructorTimetable_Load(object sender, EventArgs e)
        {
            loadtable();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked)
            {
                listView1.Items.Clear();
                hidestimeoff();
            }
            else
            {
                listView1.Items.Clear();
                loadtable();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string id = "";


            foreach (ListViewItem eachItem in listView1.SelectedItems)
            {
                id = listView1.SelectedItems[0].Text;
                //label14.Text = carid;

                SQL.executeQuery($"Delete From Timeslot Where Id = '{id}' AND Timeoff = 'Confirmed' OR Timeoff = 'Requested'"); //sql query

                listView1.Items.Clear();
                loadtable();
                checkBox1.Checked = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure this appointment has been completed?", "Warning!", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                string IdD = "";

                foreach (ListViewItem eachItem in listView1.SelectedItems)
                {
                    IdD = listView1.SelectedItems[0].Text;

                    //Client = (listView1.GetItemAt.(Position4));
                    /*string Date = listView1.SelectedItems[0].SubItems[1].Text;
                    string Time = listView1.SelectedItems[0].SubItems[2].Text;
                    string Instructor = listView1.SelectedItems[0].SubItems[3].Text;
                    string Client = listView1.SelectedItems[0].SubItems[4].Text;
                    string Status = listView1.SelectedItems[0].SubItems[6].Text;
                    string Type = listView1.SelectedItems[0].SubItems[7].Text;
                    string Cost = "";
                    if (Status == "Booked")
                    {
                        if (Type == "Beginner")
                        {
                            Cost = "$200";
                        }

                        if (Type == "Intermediate")
                        {
                            Cost = "$150";
                        }

                        if (Type == "Advanced")
                        {
                            Cost = "$100";
                        }*/

                        SQL.executeQuery($"UPDATE Timeslot SET Status = 'Driven' WHERE Id = '{IdD}' AND Status IS NOT NULL");

                        listView1.Items.Clear();
                        loadtable();
                        checkBox1.Checked = false;

                        /*string text = ($"---- DIA Driving Instruction Academy ----\r\n\r\nHello {Client},\r\n\r\nThis is your driving lesson information along with your outstanding bill:\r\n\r\n{Type} lesson with {Instructor} at {Time} on {Date}.\r\nAmount due: {Cost}.\r\n\r\n We'll see you next time!");
                        System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\" + Client + "_" + Date + "_" + Time + "_DrivingAppointment.txt");
                        file.WriteLine(text);
                        file.Close();*/
                    /*}
                    else if (Status == "Driven")
                    {
                        MessageBox.Show("This appointment has already been completed.");
                    }
                    else if (Status == "Paid")
                    {
                        MessageBox.Show("This appointment has already been completed and the bill has been paid.");
                    }
                    else
                    {
                        MessageBox.Show("That is a Time Off request. No appointment is booked for you at this time.");
                    }*/
                }

                con.Close();
            }
        }
    }
}
