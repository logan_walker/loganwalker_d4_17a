﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class AdminRegisterPage : Form
    {
        public AdminRegisterPage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Registers the new user as long as all textboxes hold text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRegister_Click(object sender, EventArgs e)
        {
            //variables to be used
            string username = "", firstname = "", lastname = "", password = "", email = "";
            string status = "admin";

            //Check that the text boxes has something typed in it using a method
            bool hasText = checkTextBoxes();
            if (!hasText)
            {
                MessageBox.Show("Please make sure all textboxes have text");
                textBoxUserName.Focus();
                return;
            }
            else if (!textBoxEmail.Text.Trim().Contains("@"))
            {
                MessageBox.Show("Please make sure email is a correct format. E.g. Someone@gmail.com ");
                textBoxUserName.Focus();
                return;
            }

            //(1) GET the data from the textboxes and store into variables created above, good to put in a try catch with error message
            try
            {
                /*YOUR CODE HERE*/
                username = textBoxUserName.Text.Trim();
                firstname = textBoxFirst.Text.Trim();
                lastname = textBoxLast.Text.Trim();
                password = textBoxPassword.Text.Trim();
                email = textBoxEmail.Text.Trim();
            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Please make sure your text is in correct format.");
                return;
            }

            //(2) Execute the INSERT statement, making sure all quotes and commas are in the correct places.
            //      Practice first on SQL Server Management Studio to make sure it is entering the correct data and in the correct format,
            //      then copy across the statement and where there are string replace the actual text for the variables stored above.
            //Example query: " INSERT INTO Users VALUES ('jkc1', 'John', 'Middle', 'Carter', 'pass1') "
            try
            {
                /*YOUR CODE HERE*/
                SQL.executeQuery("INSERT INTO Admin (UserName, FirstName, LastName, Password, Email) VALUES ('" + username + "', '" + firstname + "', '" + lastname + "', '" + password + "', '" + email + "')");
                SQL.executeQuery("INSERT INTO Users (UserName, FirstName, LastName, Password, Email, status) VALUES ('" + username + "', '" + firstname + "', '" + lastname + "', '" + password + "', '" + email + "', '" + status + "')");
            }
            catch (Exception)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }



            //success message for the user to know it worked
            MessageBox.Show("Successfully Registered: " + firstname + " " + lastname + ". Your username is: " + username);
            
            this.Close();
        }

        /// <summary>
        /// Clears all text boxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClear_Click(object sender, EventArgs e)
        {
            initialiseTextBoxes();
        }

        /// <summary>
        /// Takes us back to the login screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        /* Methods go below here */

        /// <summary>
        /// Checks if they textboxes have data in them
        /// </summary>
        /// <returns>TRUE if all hold text, but FALSE if at least one does not hold data</returns>
        private bool checkTextBoxes()
        {
            bool holdsData = true;
            //go through all of the controls
            foreach (Control c in this.Controls)
            {
                //if its a textbox, but doesnt matter if its middle textbox
                if (c is TextBox && (c != textBoxLast))
                {
                    //If it is not the case that it is empty
                    if ("".Equals((c as TextBox).Text.Trim()))
                    {
                        //set boolean to false because on textbox is empty
                        holdsData = false;
                    }
                }
            }
            //returns true or false based on if data is in all text boxs or not
            return holdsData;
        }
        /// <summary>
        /// Initialises all textboxes to blank text
        /// Does nothing in terms of re-focusing
        /// </summary>
        private void initialiseTextBoxes()
        {
            //goes through and clears all of the textboxes
            foreach (Control c in this.Controls)
            {
                //if the it is a textbox
                if (c is TextBox)
                {
                    //clear the text box
                    (c as TextBox).Clear();
                }
            }
            //focus on first text box
            textBoxUserName.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string UserName = "";
            UserName = textBoxUserName.Text;

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = INTRO_USERS; Integrated Security = True"))
            {
                SqlCommand sqlCmd = new SqlCommand($"SELECT UserName FROM Users WHERE UserName = '{UserName}'", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                while (sqlReader.Read())
                {
                    if (UserName == "")
                    {
                        MessageBox.Show("That is available.");
                    }
                    else
                    {
                        MessageBox.Show("Username unavalible. Please Pick Another");
                        return;
                    }
                }
                sqlReader.Close();
            }
        }
    }
}