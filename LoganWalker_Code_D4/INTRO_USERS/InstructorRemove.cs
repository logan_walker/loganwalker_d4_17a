﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class InstructorRemove : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS;Integrated Security=True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader read1;

        public InstructorRemove()
        {
            InitializeComponent();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Ignore
        }

        public void DisplayInstructors()
        {
            con.Open();
            cmd.CommandText = ($"select UserName, FirstName, LastName, Email from Instructor");
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();

            while (read1.Read())
            {
                listView1.View = View.Details;

                ListViewItem item = new ListViewItem(read1["UserName"].ToString());
                item.SubItems.Add(read1["FirstName"].ToString());
                item.SubItems.Add(read1["LastName"].ToString());
                item.SubItems.Add(read1["Email"].ToString());
                listView1.Items.Add(item);
            }
            con.Close();
        }

        private void InstructorRemove_Load(object sender, EventArgs e)
        {
            DisplayInstructors();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to remove this Instructor from the Database?", "Warning!", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                string UserName = "";

                foreach (ListViewItem eachItem in listView1.SelectedItems)
                {
                    UserName = listView1.SelectedItems[0].Text;
                    // label14.Text = carid; // to test if variable is passed through

                    SQL.executeQuery($"Delete From Instructor Where UserName = '{UserName}'"); //sql query
                    SQL.executeQuery($"Delete From Users Where UserName = '{UserName}'"); //sql query

                    listView1.Items.Remove(eachItem); // deletes item from listview
                }
            }
        }
    }
}
