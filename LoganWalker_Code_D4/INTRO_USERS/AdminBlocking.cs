﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class AdminBlocking : Form
    {
        public AdminBlocking(string CurrentUser)
        {
            InitializeComponent();

            label9.Text = CurrentUser;

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = INTRO_USERS; Integrated Security = True"))
            {
                SqlCommand sqlCmd2 = new SqlCommand("SELECT FirstName, LastName FROM Instructor WHERE FirstName != 'Spare'", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader2 = sqlCmd2.ExecuteReader();

                // name = SQL.read[2].ToString() + SQL.read[3].ToString();

                while (sqlReader2.Read())
                {
                    comboBoxInstructor.Items.Add(sqlReader2["FirstName"].ToString() + " " + (sqlReader2["LastName"].ToString()));
                }
                sqlReader2.Close();
            }
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            //You should have seen this from the register page, same code to switch forms.
            Hide();
            LoginPage login = new LoginPage();
            login.ShowDialog();
            Close();
        }

        /// <summary>
        /// Gets the social media id based on the social media name
        /// </summary>
        /// <param name="socialMedia">The name of the social media from combo box</param>
        /// <returns>The ID of the social media from database, blank string returned if not in database</returns>


        private void ClientBookTimes_Load(object sender, EventArgs e)
        {
            ResetColor();
        }

        public static string timeslot;
        public static string day;
        public static string instructor;
        public static string client;

        private void button16_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "8.00am-9.00am";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button16.BackColor = Color.DarkGray;
        }

        public void button1_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "7.00am-8.00am";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button1.BackColor = Color.DarkGray;
        }

        public void button8_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "9.00am-10.00am";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button8.BackColor = Color.DarkGray;
        }

        public void button3_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "10.00am-11.00am";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button3.BackColor = Color.DarkGray;
        }

        public void button10_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "11.00am-12.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button10.BackColor = Color.DarkGray;
        }

        public void button6_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "12.00pm-1.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button6.BackColor = Color.DarkGray;
        }

        public void button5_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "1.00pm-2.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button5.BackColor = Color.DarkGray;
        }

        public void button4_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "2.00pm-3.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button4.BackColor = Color.DarkGray;
        }

        public void button11_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "3.00pm-4.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button11.BackColor = Color.DarkGray;
        }

        public void button7_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "4.00pm-5.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button7.BackColor = Color.DarkGray;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "5.00pm-6.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button15.BackColor = Color.DarkGray;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "6.00pm-7.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button14.BackColor = Color.DarkGray;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "7.00pm-8.00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button12.BackColor = Color.DarkGray;
        }

        public void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            ResetColor();

            string Date = "", selectDay = "";
            Date = dateTimePicker1.Text.ToString();
            selectDay = Date;

            if (selectDay[0] == 'S' && selectDay[1] == 'u')
            {
                button1.Visible = false;
                button3.Visible = false;
                button6.Visible = false;
                button8.Visible = false;
                button11.Visible = false;
                button10.Visible = false;
                button7.Visible = false;
                button5.Visible = false;
                button4.Visible = false;
                button16.Visible = false;
                button15.Visible = false;
                button14.Visible = false;
                button12.Visible = false;
                button13.Visible = false;
                button9.Visible = false;
                label2.Visible = false;
                label6.Visible = false;
                label7.Visible = false;
                labelSunday.Visible = true;
            }
            else
            {
                button1.Visible = true;
                button3.Visible = true;
                button6.Visible = true;
                button8.Visible = true;
                button11.Visible = true;
                button10.Visible = true;
                button7.Visible = true;
                button5.Visible = true;
                button4.Visible = true;
                button16.Visible = true;
                button15.Visible = true;
                button14.Visible = true;
                button12.Visible = true;
                button13.Visible = true;
                button9.Visible = true;
                label2.Visible = true;
                label6.Visible = true;
                label7.Visible = true;
                labelSunday.Visible = false;
            }

        }
        public void ResetColor()
        {
            button1.BackColor = Color.FromKnownColor(KnownColor.Control);
            button3.BackColor = Color.FromKnownColor(KnownColor.Control);
            button6.BackColor = Color.FromKnownColor(KnownColor.Control);
            button8.BackColor = Color.FromKnownColor(KnownColor.Control);
            button11.BackColor = Color.FromKnownColor(KnownColor.Control);
            button10.BackColor = Color.FromKnownColor(KnownColor.Control);
            button7.BackColor = Color.FromKnownColor(KnownColor.Control);
            button5.BackColor = Color.FromKnownColor(KnownColor.Control);
            button4.BackColor = Color.FromKnownColor(KnownColor.Control);
            button9.BackColor = Color.FromKnownColor(KnownColor.Control);
            button13.BackColor = Color.FromKnownColor(KnownColor.Control);
            button16.BackColor = Color.FromKnownColor(KnownColor.Control);
            button15.BackColor = Color.FromKnownColor(KnownColor.Control);
            button14.BackColor = Color.FromKnownColor(KnownColor.Control);
            button12.BackColor = Color.FromKnownColor(KnownColor.Control);
            buttonLogin.BackColor = Color.FromKnownColor(KnownColor.Control);
        }

        public void button13_Click(object sender, EventArgs e) //submit button
        {
            //variables to be used
            string Time = "", Date = "", InstructorName = "", CurrentUser = "Confirmed";

            Time = timeslot;
            Date = dateTimePicker1.Text.ToString();
            InstructorName = comboBoxInstructor.Text;

            string TimeI = "", DateI = "", InstructorNameI = "", CurrentUserI = ""; // TEST CODE

            TimeI = Time;
            DateI = Date;
            InstructorNameI = InstructorName;
            CurrentUserI = CurrentUser;

            if (label7.Text == "")
            {
                MessageBox.Show("Error, Missing instructor or time/date.");
            }
            else
            { // TEST CODE STARTS ------------------------------------------------------------------------

                using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = INTRO_USERS; Integrated Security = True"))
                {
                    // System.Diagnostics.Debug.WriteLine($"########    SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}' AND Client = '{CurrentUserI}'");


                    SqlCommand sqlCmd = new SqlCommand($"SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}' AND Timeoff = '{CurrentUserI}'", sqlConnection);
                    sqlConnection.Open();
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    // name = SQL.read[2].ToString() + SQL.read[3].ToString();  (Time, Date, InstructorName, Client)

                    while (sqlReader.Read())
                    {
                        if (sqlReader.HasRows)
                        {
                            TimeI = sqlReader.GetString(1);
                            DateI = sqlReader.GetString(2);
                            InstructorNameI = sqlReader.GetString(3);
                            //CurrentUserI = sqlReader.GetString(4);

                            if (TimeI == "" && DateI == "" && InstructorNameI == "" && CurrentUserI == "")
                            {

                                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + Time + "', '" + Date + "','" + InstructorName + "', '" + CurrentUser + "')");
                                //success message for the user to know it worked
                                MessageBox.Show("Time successfully booked off for " + InstructorName + " - Booked off time is: " + Date + " at " + Time + " With " + InstructorName);


                            }
                            else
                            {
                                MessageBox.Show("Time unavalible. Please Pick Another");
                                return;
                            }
                        }
                    }
                    sqlReader.Close();
                }
                // CheckInput(); TEST CODE ENDS ---------------------------------------------------------------
                try
                {
                    SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + Time + "', '" + Date + "','" + InstructorName + "', '" + CurrentUser + "')");
                    //success message for the user to know it worked
                    MessageBox.Show("Time successfully booked off for " + InstructorName + " - Time booked off is: " + Date + " at " + Time);
                }
                catch (Exception)
                {
                    MessageBox.Show("Time unavalible.");
                    return;
                }
            }
        }

        public void button9_Click(object sender, EventArgs e) // BLOCK OUT FULL DAY
        {
            //variables to be used
            string Time = "", Date = "", InstructorName = "", CurrentUser = "Confirmed";

            Time = timeslot;
            Date = dateTimePicker1.Text.ToString();
            InstructorName = comboBoxInstructor.Text;

            string DateI = "", InstructorNameI = "", CurrentUserI = ""; // TEST CODE

            DateI = Date;
            InstructorNameI = InstructorName;
            CurrentUserI = CurrentUser;


            // TEST CODE STARTS ------------------------------------------------------------------------

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = INTRO_USERS; Integrated Security = True"))
            {
                // System.Diagnostics.Debug.WriteLine($"########    SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}' AND Client = '{CurrentUserI}'");


                SqlCommand sqlCmd = new SqlCommand($"SELECT * FROM Timeslot WHERE Date = '{DateI}' AND InstructorName = '{InstructorNameI}' AND Timeoff = '{CurrentUserI}'", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                // name = SQL.read[2].ToString() + SQL.read[3].ToString();  (Time, Date, InstructorName, Client)

                while (sqlReader.Read())
                {
                    if (sqlReader.HasRows)
                    {
                        DateI = sqlReader.GetString(2);
                        InstructorNameI = sqlReader.GetString(3);
                        //CurrentUserI = sqlReader.GetString(4);


                        if (DateI == "" && InstructorNameI == "" && CurrentUserI == "")
                        {

                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "7.00am-8.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "8.00am-9.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "9.00am-10.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "10.00am-11.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "11.00am-12.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "12.00am-1.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "1.00pm-2.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "2.00pm-3.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "3.00pm-4.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "4.00pm-5.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "5.00pm-6.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "6.00pm-7.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "7.00pm-8.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                            MessageBox.Show("Successfully booked out full day for " + InstructorName + " on: " + Date);

                        }
                        else
                        {
                            MessageBox.Show(" " + InstructorName + " already has time booked off on this day. Full day cannot be booked off.");
                            return;
                        }
                    }

                }
                sqlReader.Close();
            }
            // CheckInput(); TEST CODE ENDS ---------------------------------------------------------------
            try
            {
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "7.00am-8.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "8.00am-9.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "9.00am-10.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "10.00am-11.00am" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "11.00am-12.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "12.00am-1.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "1.00pm-2.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "2.00pm-3.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "3.00pm-4.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "4.00pm-5.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "5.00pm-6.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "6.00pm-7.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Timeoff) VALUES ('" + "7.00pm-8.00pm" + "', '" + Date + "', '" + InstructorName + "', '" + CurrentUser + "' )");
                MessageBox.Show("Successfully booked off full day for " + InstructorName + " on: " + Date);
            }
            catch (Exception)
            {
                MessageBox.Show("Time unavalible.");
                return;
            }
        }

        private void buttonLogin_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
