﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class AdminPage : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS;Integrated Security=True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader read1;
        SqlDataReader read2;
        SqlDataReader read3;
        SqlDataReader read4;
        SqlDataReader read6;

        public string currentuser = "";

        public void AdminTimetable()
        {
            con.Open();
            cmd.CommandText = ("select ID, Date, Time, InstructorName, Client, TimeOff, Status, Type from TIMESLOT");
            cmd.Connection = con;
            read2 = cmd.ExecuteReader();

            while (read2.Read())
            {
                listView1.View = View.Details;

                ListViewItem item = new ListViewItem(read2["ID"].ToString());
                item.SubItems.Add(read2["Date"].ToString());
                item.SubItems.Add(read2["Time"].ToString());
                item.SubItems.Add(read2["InstructorName"].ToString());
                item.SubItems.Add(read2["Client"].ToString());
                item.SubItems.Add(read2["TimeOff"].ToString());
                item.SubItems.Add(read2["Status"].ToString());
                item.SubItems.Add(read2["Type"].ToString());

                listView1.Items.Add(item);
            }
            con.Close();
        }

        public void CarTimetable()
        {
            adminCarListView.Items.Clear();

            con.Open();
            cmd.CommandText = ($"SELECT License, InstructorName FROM Assigned");
            cmd.Connection = con;
            read6 = cmd.ExecuteReader();

            while (read6.Read())
            {
                adminCarListView.View = View.Details;

                ListViewItem item = new ListViewItem(read6["License"].ToString());
                item.SubItems.Add(read6["InstructorName"].ToString());
                
                adminCarListView.Items.Add(item);
            }
            con.Close();
        }

        public void FilterComboboxes()
        {
            con.Open();
            SqlCommand sqlCmd3 = new SqlCommand("SELECT FirstName, LastName FROM Instructor WHERE FirstName != 'Spare'", con);
            SqlDataReader sqlReader3 = sqlCmd3.ExecuteReader();
            while (sqlReader3.Read())
            {
                comboBox2.Items.Add(sqlReader3["FirstName"].ToString() + " " + (sqlReader3["LastName"].ToString()));
            }
            sqlReader3.Close();

            SqlCommand sqlCmd4 = new SqlCommand("SELECT Name FROM Client", con);
            SqlDataReader sqlReader4 = sqlCmd4.ExecuteReader();
            while (sqlReader4.Read())
            {
                comboBox3.Items.Add(sqlReader4["Name"].ToString());
            }
            sqlReader4.Close();
            con.Close();
        }

        public void CarComboboxes()
        {
            con.Open();
            cmd.CommandText = ($"SELECT License FROM Car");
            cmd.Connection = con;
            read3 = cmd.ExecuteReader();

            while (read3.Read())
            {
                comboCar.Items.Add(read3["License"].ToString());
            }
            con.Close();
        }

        public void InstructorComboboxes()
        {
            con.Open();
            cmd.CommandText = ($"SELECT FirstName, LastName FROM Instructor");
            cmd.Connection = con;
            read4 = cmd.ExecuteReader();

            while (read4.Read())
            {
                comboInstructor.Items.Add(read4["FirstName"].ToString() + " " + (read4["LastName"].ToString()));
            }
            con.Close();
        }

        public void hidestuff()
        {

            listView1.Items.Clear();

            con.Open();
            cmd.CommandText = ($"select ID, Date, Time, InstructorName, Client, TimeOff, Status, Type from TIMESLOT WHERE TimeOff IS NULL");
            cmd.Connection = con;
            read6 = cmd.ExecuteReader();

            while (read6.Read())
            {
                listView1.View = View.Details;

                ListViewItem item = new ListViewItem(read6["ID"].ToString());
                item.SubItems.Add(read6["Date"].ToString());
                item.SubItems.Add(read6["Time"].ToString());
                item.SubItems.Add(read6["InstructorName"].ToString());
                item.SubItems.Add(read6["Client"].ToString());
                item.SubItems.Add(read6["TimeOff"].ToString());
                item.SubItems.Add(read6["Status"].ToString());
                item.SubItems.Add(read2["Type"].ToString());

                listView1.Items.Add(item);
            }
            con.Close();

        }

        public AdminPage(string CurrentUser)
        {
            InitializeComponent();
            
            currentuser = CurrentUser;
            label9.Text = currentuser;
        }

        private void buttonBooking_Click(object sender, EventArgs e)
        {
            AdminBooking browsePage = new AdminBooking(currentuser);
            browsePage.ShowDialog();
        }

        private void buttonBlocking_Click(object sender, EventArgs e)
        {
            AdminBlocking browsePage = new AdminBlocking(currentuser);
            browsePage.ShowDialog();
        }

        private void AdminPage_Load(object sender, EventArgs e)
        {
            AdminTimetable();
            CarTimetable();

            FilterComboboxes();
            InstructorComboboxes();
            CarComboboxes();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            adminCarListView.Items.Clear();
            AdminTimetable();
            CarTimetable();
            checkBox1.Checked = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RegisterPage register = new RegisterPage();
            register.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            InstructorRegisterPage register = new InstructorRegisterPage();
            register.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AdminRegisterPage register = new AdminRegisterPage();
            register.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e) //Assign car button
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to make this car assignment?", "Warning!", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                string InstructorName = "", License = "";

                InstructorName = comboInstructor.Text;
                License = comboCar.Text;

                string InstructorNameI = "", LicenseI = "";

                InstructorNameI = InstructorName;
                LicenseI = License;

                if (comboCar.SelectedItem == null || comboInstructor.Text == null)
                {
                    MessageBox.Show("Error, Missing instructor or car.");
                }
                else
                { // TEST CODE STARTS ------------------------------------------------------------------------

                    using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = INTRO_USERS; Integrated Security = True"))
                    {
                        // System.Diagnostics.Debug.WriteLine($"########    SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}' AND Client = '{CurrentUserI}'");


                        SqlCommand sqlCmd = new SqlCommand($"SELECT * FROM Assigned WHERE License = '{LicenseI}'", sqlConnection);
                        sqlConnection.Open();
                        SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                        // name = SQL.read[2].ToString() + SQL.read[3].ToString();  (Time, Date, InstructorName, Client)

                        while (sqlReader.Read())
                        {
                            if (sqlReader.HasRows)
                            {
                                LicenseI = sqlReader.GetString(0);
                                InstructorNameI = sqlReader.GetString(1);

                                if (InstructorNameI == "" && LicenseI == "")
                                {
                                    SQL.executeQuery("INSERT INTO Assigned (License, InstructorName) VALUES ( '" + License + "', '" + InstructorName + "')");
                                    //success message for the user to know it worked
                                    MessageBox.Show("Assigned Vehicle " + License + " To " + InstructorName);
                                }
                                else
                                {
                                    MessageBox.Show("Car already assigned.");
                                    return;
                                }
                            }
                        }

                        sqlReader.Close();

                        
                    }
                    // CheckInput(); TEST CODE ENDS ---------------------------------------------------------------
                    try
                    {
                        SQL.executeQuery("INSERT INTO Assigned (License, InstructorName) VALUES ('" + License + "', '" + InstructorName + "')");
                        //success message for the user to know it worked
                        MessageBox.Show("Assigned Vehicle " + License + " To " + InstructorName);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Car already assigned.");
                        return;
                    }
                }

                //SQL.executeQuery("INSERT INTO Assigned (License, InstructorName) VALUES ('" + License + "', '" + InstructorName + "')");
                //success message for the user to know it worked
                //CarTimetable();
                //MessageBox.Show("Assigned Vehicle " + License + " To " + InstructorName);
            }

            listView1.Items.Clear();
            adminCarListView.Items.Clear();
            AdminTimetable();
            CarTimetable();
        }

        private void button5_Click(object sender, EventArgs e) //Delete assigned car button
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to remove this car assignment?", "Warning!", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                string carid = "";

                foreach (ListViewItem eachItem in adminCarListView.SelectedItems)
                {
                    carid = adminCarListView.SelectedItems[0].Text;
                    //label14.Text = carid;

                    SQL.executeQuery($"Delete From Assigned Where License = '{carid}' "); //sql query
                    adminCarListView.Items.Remove(eachItem); // deletes item from listview
                }
            }
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            //hides this form currently on
            Hide();
            //is the login page as a new object               
            LoginPage login = new LoginPage();
            //Shows the login page window
            login.ShowDialog();
            //closes the current open windows so its only the new one showing
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                listView1.Items.Clear();
                hidestuff();
            }
            else
            {
                listView1.Items.Clear();
                AdminTimetable();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete this timeslot? This cannot be undone.", "Warning!", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                string IdD = "";

                foreach (ListViewItem eachItem in listView1.SelectedItems)
                {
                    IdD = listView1.SelectedItems[0].Text;

                    SQL.executeQuery($"Delete From Timeslot Where Id = '{IdD}'"); //sql query 
                    listView1.Items.Remove(eachItem); // deletes item from listview
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            string date = dateTimePicker1.Text.ToString();
            con.Open();
            cmd.CommandText = $"SELECT Id, Time, Date, InstructorName, Client, TimeOff, Status, Type FROM TimeSlot WHERE Date = '{date}'";
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();
            while (read1.Read())
            {
                listView1.View = View.Details;
                ListViewItem item = new ListViewItem(read1["Id"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["TimeOff"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Type"].ToString());
                listView1.Items.Add(item);
            }
            con.Close();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            string instructor = comboBox2.Text;
            con.Open();
            cmd.CommandText = $"SELECT Id, Time, Date, InstructorName, Client, TimeOff, Status, Type FROM TimeSlot WHERE InstructorName = '{instructor}'";
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();
            while (read1.Read())
            {
                listView1.View = View.Details;
                ListViewItem item = new ListViewItem(read1["Id"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["TimeOff"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Type"].ToString());
                listView1.Items.Add(item);
            }
            con.Close();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            string client = comboBox3.Text;
            con.Open();
            cmd.CommandText = $"SELECT Id, Time, Date, InstructorName, Client, TimeOff, Status, Type FROM TimeSlot WHERE Client = '{client}'";
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();
            while (read1.Read())
            {
                listView1.View = View.Details;
                ListViewItem item = new ListViewItem(read1["Id"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["TimeOff"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Type"].ToString());
                listView1.Items.Add(item);
            }
            con.Close();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            string date = dateTimePicker1.Text.ToString();
            string instructor = comboBox2.Text;
            string client = comboBox3.Text;
            string timeoff = comboBox1.Text;
            string status = comboBox4.Text;
            
            con.Open();
            cmd.CommandText = $"SELECT Id, Time, Date, InstructorName, Client, TimeOff, Status FROM TimeSlot WHERE Date = '{date}' AND InstructorName = '{instructor}' AND Client = '' AND TimeOff = '{timeoff}' AND Status = '{status}'";
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();
            while (read1.Read())
            {
                listView1.View = View.Details;
                ListViewItem item = new ListViewItem(read1["Id"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["TimeOff"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                listView1.Items.Add(item);
            }

            con.Close();

        }

        private void button12_Click(object sender, EventArgs e)
        {
            comboBox2.SelectedItem = null;
            comboBox3.SelectedItem = null;
            comboBox1.SelectedItem = null;
            comboBox4.SelectedItem = null;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            //hides this form currently on
            Hide();
            //is the login page as a new object               
            AdminPage login = new AdminPage(currentuser);
            //Shows the login page window
            login.ShowDialog();
            //closes the current open windows so its only the new one showing
            this.Close();
        }

        private void button14_Click(object sender, EventArgs e) //Button which approves the instructors time off request
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to approve this Instructors Time Off request?", "Warning!", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                string IdD = "", timeoff = "";

                foreach (ListViewItem eachItem in listView1.SelectedItems)
                {
                    IdD = listView1.SelectedItems[0].Text;
                    timeoff = listView1.SelectedItems[0].SubItems[5].Text;

                    if (timeoff == "Requested")
                    {
                        SQL.executeQuery($"UPDATE Timeslot SET Timeoff = 'Confirmed' WHERE Id = '{IdD}' AND Timeoff is NOT NULL");
                        listView1.Items.Clear();
                        AdminTimetable();
                        AdminTimetable();
                        CarTimetable();
                        checkBox1.Checked = false;
                    }
                    else if (timeoff == "Confirmed")
                    {
                        MessageBox.Show("This Time Off request has already been approved.");
                    }
                    else if (timeoff == "Declined")
                    {
                        DialogResult dialog = MessageBox.Show("This Time Off request has already been declined. Do you still want to Confirm this Time Off?", "Warning!", MessageBoxButtons.YesNo);
                        if (dialog == DialogResult.Yes)
                        {
                            SQL.executeQuery($"UPDATE Timeslot SET Timeoff = 'Confirmed' WHERE Id = '{IdD}' AND Timeoff is NOT NULL");
                            listView1.Items.Clear();
                            AdminTimetable();
                            AdminTimetable();
                            CarTimetable();
                            checkBox1.Checked = false;
                            break;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error. This is not a Time Off slot.");
                    }
                }
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            string timeoff = comboBox1.Text;
            con.Open();
            cmd.CommandText = $"SELECT Id, Time, Date, InstructorName, Client, Timeoff, Status, Type FROM TimeSlot WHERE Timeoff = '{timeoff}'";
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();
            while (read1.Read())
            {
                listView1.View = View.Details;
                ListViewItem item = new ListViewItem(read1["Id"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["Timeoff"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Type"].ToString());
                listView1.Items.Add(item);
            }
            con.Close();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            AdminRemove register = new AdminRemove();
            register.ShowDialog();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            InstructorRemove register = new InstructorRemove();
            register.ShowDialog();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            string status = comboBox4.Text;
            con.Open();
            cmd.CommandText = $"SELECT Id, Time, Date, InstructorName, Client, Timeoff, Status, Type FROM TimeSlot WHERE Status = '{status}'";
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();
            while (read1.Read())
            {
                listView1.View = View.Details;
                ListViewItem item = new ListViewItem(read1["Id"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["Timeoff"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Type"].ToString());
                listView1.Items.Add(item);
            }
            con.Close();
        }

        private void button11_Click_1(object sender, EventArgs e) //Button which confirms that a bill has been paid by the client
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to confirm that this bill has been paid?", "Warning!", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                string IdD = "", status = "";

                foreach (ListViewItem eachItem in listView1.SelectedItems)
                {
                    IdD = listView1.SelectedItems[0].Text;
                    status = listView1.SelectedItems[0].SubItems[6].Text;

                    if (status == "Driven")
                    {
                        MessageBox.Show("A bill has not yet been sent for this lesson.");
                    }
                    else if (status == "Bill sent")
                    {
                        SQL.executeQuery($"UPDATE Timeslot SET Status = 'Paid' WHERE Id = '{IdD}'");
                        listView1.Items.Clear();
                        adminCarListView.Items.Clear();
                        AdminTimetable();
                        CarTimetable();
                    }
                    else if (status == "Booked")
                    {
                        MessageBox.Show("This appointment has not been completed yet, no bill has been sent.");
                    }
                    else if (status == "Paid")
                    {
                        MessageBox.Show("This appointment's bill has already been paid.");
                    }
                    else
                    {
                        MessageBox.Show("This is a Time Off slot. No bill is linked to this time slot.");
                    }
                }
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            Statistics stats = new Statistics();
            stats.ShowDialog();
        }

        private void button20_Click(object sender, EventArgs e) // Button which declines instructor time off requested
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to decline this requested time off?", "Warning!", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string id = "";
                string timeoff;

                foreach (ListViewItem eachItem in listView1.SelectedItems)
                {
                    id = listView1.SelectedItems[0].Text;
                    timeoff = listView1.SelectedItems[0].SubItems[5].Text;

                    if (timeoff == "Requested")
                    {
                        SQL.executeQuery($"UPDATE Timeslot SET Timeoff = 'Declined' WHERE Id = '{id}' AND Timeoff is NOT NULL"); //sql query
                        listView1.Items.Clear();
                        adminCarListView.Items.Clear();
                        AdminTimetable();
                        CarTimetable();
                    }
                    else if (timeoff == "Declined")
                    {
                        MessageBox.Show("This time off has already been declined.");
                    }
                    else if (timeoff == "Confirmed")
                    {
                        DialogResult dialog = MessageBox.Show("This timeoff has already been approved. Do you really want to decline it?", "Warning!", MessageBoxButtons.YesNo);
                        if (dialog == DialogResult.Yes)
                        {
                            SQL.executeQuery($"UPDATE Timeslot SET Timeoff = 'Declined' WHERE Id = '{id}' AND Timeoff is NOT NULL"); //sql query
                            listView1.Items.Clear();
                            AdminTimetable();
                            break;
                        }
                    }
                    else
                    {
                        MessageBox.Show($"Sorry, this is an appointment slot not a timeoff request.");
                    }
                }
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to send a bill for this lesson?", "Warning!", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                string IdD = "", status = "";

                foreach (ListViewItem eachItem in listView1.SelectedItems)
                {
                    IdD = listView1.SelectedItems[0].Text;
                    status = listView1.SelectedItems[0].SubItems[6].Text;

                    if (status == "Driven")
                    {
                        string Date = listView1.SelectedItems[0].SubItems[1].Text;
                        string Time = listView1.SelectedItems[0].SubItems[2].Text;
                        string Instructor = listView1.SelectedItems[0].SubItems[3].Text;
                        string Client = listView1.SelectedItems[0].SubItems[4].Text;
                        string Status = listView1.SelectedItems[0].SubItems[6].Text;
                        string Type = listView1.SelectedItems[0].SubItems[7].Text;
                        string Cost = "";

                        if (Type == "Beginner")
                        {
                            Cost = "$200";
                        }

                        if (Type == "Intermediate")
                        {
                            Cost = "$150";
                        }

                        if (Type == "Advanced")
                        {
                            Cost = "$100";
                        }

                        SQL.executeQuery($"UPDATE Timeslot SET Status = 'Bill sent' WHERE Id = '{IdD}'"); //sql query
                        listView1.Items.Clear();
                        adminCarListView.Items.Clear();
                        AdminTimetable();
                        CarTimetable();

                        string text = ($"---- DIA Driving Instruction Academy ----\r\n\r\nHello {Client},\r\n\r\nThis is your driving lesson information along with your outstanding bill:\r\n\r\n{Type} lesson with {Instructor} at {Time} on {Date}.\r\nAmount due: {Cost}.\r\n\r\n We'll see you next time!");
                        System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\" + Client + "_" + Date + "_" + Time + "_DrivingAppointment.txt");
                        file.WriteLine(text);
                        file.Close();

                    }
                    else if (status == "Booked")
                    {
                        MessageBox.Show("This appointment must be completed before a bill can be sent.");
                    }
                    else if (status == "Paid")
                    {
                        MessageBox.Show("This appointment's bill has already been sent and paid.");
                    }
                    else
                    {
                        MessageBox.Show("This is a Time Off slot. No bill required.");
                    }
                }
            }
        }
    }
}
