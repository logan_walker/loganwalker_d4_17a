﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class Statistics : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS;Integrated Security=True");

        SqlCommand cmd = new SqlCommand();
        public Statistics()
        {
            InitializeComponent();

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = INTRO_USERS; Integrated Security = True"))
            {
                SqlCommand sqlCmd2 = new SqlCommand("SELECT FirstName, LastName FROM Instructor WHERE FirstName != 'Spare'", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader2 = sqlCmd2.ExecuteReader();

                // name = SQL.read[2].ToString() + SQL.read[3].ToString();

                while (sqlReader2.Read())
                {
                    comboBox1.Items.Add(sqlReader2["FirstName"].ToString() + " " + (sqlReader2["LastName"].ToString()));
                }
                sqlReader2.Close();
            }
        }

        public void loadStatistics()
        {
            con.Open();
            cmd.CommandText = ($"Select count(*) From Users");
            cmd.Connection = con;
            label1.Text = "Total Registered Users: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Admin");
            cmd.Connection = con;
            label2.Text = "Total Administrators: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Instructor WHERE FirstName != 'Spare'");
            cmd.Connection = con;
            label3.Text = "Total Instructors: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Client");
            cmd.Connection = con;
            label4.Text = "Total Clients: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Client where Type = 'Beginner'");
            cmd.Connection = con;
            label5.Text = "Total Beginner Clients: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Client where Type = 'Intermediate'");
            cmd.Connection = con;
            label6.Text = "Total Intermediate Clients: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Client where Type = 'Advanced'");
            cmd.Connection = con;
            label7.Text = "Total Advanced Clients: " + cmd.ExecuteScalar().ToString();
            con.Close();





            con.Open();
            cmd.CommandText = ($"Select count(*) From Timeslot Where Client != 'Blocked Out'  ");
            cmd.Connection = con;
            label8.Text = "Total Appointments: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Timeslot Where Timeoff IS NOT NULL  ");
            cmd.Connection = con;
            label9.Text = "Total times off requests: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) from timeslot where date like '%June%'");
            cmd.Connection = con;
            label10.Text = "Total appointments this month: " + cmd.ExecuteScalar().ToString();
            con.Close();

        }

        private void Statistics_Load(object sender, EventArgs e)
        {
            loadStatistics();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            con.Open();
            string date = dateTimePicker1.Text.ToString();
            cmd.CommandText = ($"Select count(*) From Timeslot Where Date = '{date}' AND Timeoff IS NULL ");
            cmd.Connection = con;
            label11.Text = "Number of appointments by day: " + cmd.ExecuteScalar().ToString();
            con.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            con.Open();
            string instructor = comboBox1.Text;
            cmd.CommandText = ($"Select count(*) From Timeslot Where InstructorName = '{instructor}' AND Timeoff IS NULL");
            cmd.Connection = con;
            label12.Text = "Number of appointments by Instructor: " + cmd.ExecuteScalar().ToString();
            con.Close();
        }
    }
}
