/*
CREATE DATABASE INTRO_USERS
GO
*/

/*
USE INTRO_USERS
GO
*/

CREATE TABLE Admin (
	UserName varchar(20) NOT NULL PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	LastName varchar(20) NOT NULL,
	Password varchar(30) NOT NULL,
	Email varchar(50) NOT NULL
	)

CREATE TABLE Document (
	ID int NOT NULL PRIMARY KEY,
	Date date NOT NULL,
	Link varchar(30) NOT NULL,
	Type varchar(30) NOT NULL
	)

CREATE TABLE Car (
	License varchar(20) NOT NULL PRIMARY KEY,
	Make varchar(20) NOT NULL
	)

CREATE TABLE Instructor (
	Username varchar(20) NOT NULL PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	LastName varchar(20) NOT NULL,
	Password varchar(30) NOT NULL,
	Email varchar(50) NOT NULL,
	Phone varchar(20) NOT NULL
	)

CREATE TABLE Users (
	Username varchar(20) NOT NULL PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	LastName varchar(20) NOT NULL,
	Password varchar(30) NOT NULL,
	Email varchar(50) NOT NULL,
	Phone varchar(20),
	CourseType varchar(30),
	Status varchar(20) NOT NULL
	)

CREATE TABLE Appointment (
	ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Notes varchar(1000)
	)

CREATE TABLE Timeslot (
	Id int IDENTITY(1,1) NOT NULL,
	Time varchar(50) NOT NULL,
	Date varchar(100) NOT NULL,
	InstructorName varchar(50) NOT NULL, 
	Client varchar(50),
	TimeOff varchar(20),
	Status varchar(20),
	Type varchar(20),
	PRIMARY KEY (Id, Time, Date)
	)


		
CREATE TABLE Client (
	Username varchar(20) NOT NULL PRIMARY KEY,
	Name varchar(50) NOT NULL,
	Password varchar(30) NOT NULL,
	Email varchar(50) NOT NULL,
	Phone varchar(20) NOT NULL,
	Type varchar(30) NOT NULL,
	--FOREIGN KEY (Type) REFERENCES ClientType
	)

	/*CREATE TABLE ClientType (
	Name varchar(30) PRIMARY KEY,
	Cost varchar(10) NOT NULL,
	Hours int NOT NULL
	)*/

/*CREATE TABLE Books (
	Client_Username varchar(20) NOT NULL,
	Appointment_Id int NOT NULL,
	PRIMARY KEY (Client_Username, Appointment_Id),
	FOREIGN KEY (Client_Username) REFERENCES Client,
	FOREIGN KEY (Appointment_Id) REFERENCES Appointment
	)

CREATE TABLE Receives (
	Client_Username varchar(20) NOT NULL,
	Doc_Id int NOT NULL,
	PRIMARY KEY (Client_Username, Doc_Id),
	FOREIGN KEY (Client_Username) REFERENCES Client,
	FOREIGN KEY (Doc_Id) REFERENCES Document
	)*/

CREATE TABLE Assigned (
	License varchar(20) NOT NULL,
	InstructorName varchar(20) NOT NULL,
	PRIMARY KEY (License, InstructorName),
	--FOREIGN KEY (License) REFERENCES Car,
	--FOREIGN KEY (Instructor_Username) REFERENCES Instructor,
	--FOREIGN KEY (Appointment_Id) REFERENCES Appointment
	)




/* Car inserts */

INSERT INTO Car VALUES ('CX98R4', 'Nissan')
INSERT INTO Car VALUES ('PL1436', 'Nissan')
INSERT INTO Car VALUES ('CNM984', 'Toyota')
INSERT INTO Car VALUES ('THN235', 'Subaru')
INSERT INTO Car VALUES ('ML6731', 'Nissan')

/* Admin inserts */

INSERT INTO Admin VALUES ('Tyler', 'Tyler', 'Dog', 'password', 'd4databasetest@gmail.com')
INSERT INTO Admin VALUES ('Benny', 'Ben', 'McDonald', 'password', 'd4databasetest@gmail.com')
INSERT INTO Admin VALUES ('Jimmy', 'James', 'Richards', 'password', 'd4databasetest@gmail.com')

INSERT INTO Users (Username, FirstName, LastName, Password, Email, Phone, CourseType, Status) VALUES ('Tyler', 'Tyler', 'Dog', 'password', 'd4databasetest@gmail.com',NULL,NULL,'admin')
INSERT INTO Users (Username, FirstName, LastName, Password, Email, Phone, CourseType, Status) VALUES ('Benny', 'Ben', 'McDonald', 'password', 'd4databasetest@gmail.com',NULL,NULL,'admin')
INSERT INTO Users (Username, FirstName, LastName, Password, Email, Phone, CourseType, Status) VALUES ('Jimmy', 'James', 'Richards', 'password', 'd4databasetest@gmail.com',NULL,NULL,'admin')

/* Client inserts */

INSERT INTO Client VALUES ('Mike', 'Michael Reese', 'password', 'd4databasetest@gmail.com', '0279765341', 'Beginner')
INSERT INTO Client VALUES ('Lee', 'Lee Kyle', 'password', 'd4databasetest@gmail.com', '0279765456', 'Intermediate')
INSERT INTO Client VALUES ('Ben', 'Ben Bruin', 'password', 'd4databasetest@gmail.com', '0279765395', 'Advanced')

INSERT INTO Users (Username, FirstName, LastName, Password, Email, Phone, CourseType, Status) VALUES ('Mike', 'Michael', 'Reese', 'password', 'd4databasetest@gmail.com', '0279765341', 'Beginner','client')
INSERT INTO Users (Username, FirstName, LastName, Password, Email, Phone, CourseType, Status) VALUES ('Lee', 'Lee', 'Kyle', 'password', 'd4databasetest@gmail.com', '0279765456', 'Intermediate','client')
INSERT INTO Users (Username, FirstName, LastName, Password, Email, Phone, CourseType, Status) VALUES ('Ben', 'Ben', 'Bruin', 'password', 'd4databasetest@gmail.com', '0279765395', 'Advanced','client')

/* Instructor inserts */

INSERT INTO Instructor VALUES ('Sarah', 'Sarah', 'Parker', 'password', 'd4databasetest@gmail.com', '0275239821')
INSERT INTO Instructor VALUES ('Jeremy', 'Jeremy', 'Bruin', 'password', 'd4databasetest@gmail.com', '0279851234')
INSERT INTO Instructor VALUES ('Liam', 'Liam', 'Watson', 'password', 'd4databasetest@gmail.com', '0275738742')
INSERT INTO Instructor VALUES ('Jeffy', 'Jeff', 'Willow', 'password', 'd4databasetest@gmail.com', '0279873476')

INSERT INTO Instructor VALUES ('Spare', 'Spare', '(Not in use)', 'Spare', 'Spare', 'Spare')

INSERT INTO Users (Username, FirstName, LastName, Password, Email, Phone, CourseType, Status) VALUES ('Sarah', 'Sarah', 'Parker', 'password', 'd4databasetest@gmail.com', '0275239821',NULL,'instructor')
INSERT INTO Users (Username, FirstName, LastName, Password, Email, Phone, CourseType, Status) VALUES ('Jeremy', 'Jeremy', 'Bruin', 'password', 'd4databasetest@gmail.com', '0279851234',NULL,'instructor')
INSERT INTO Users (Username, FirstName, LastName, Password, Email, Phone, CourseType, Status) VALUES ('Liam', 'Liam', 'Watson', 'password', 'd4databasetest@gmail.com', '0275738742',NULL,'instructor')
INSERT INTO Users (Username, FirstName, LastName, Password, Email, Phone, CourseType, Status) VALUES ('Jeffy', 'Jeff', 'Willow', 'password', 'd4databasetest@gmail.com', '0279873476',NULL,'instructor')

/* client appointments */

INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('8.00am-9.00am', 'Monday, 12 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Driven', 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('9.00am-10.00am', 'Monday, 12 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Driven', 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('10.00am-11.00am', 'Monday, 12 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Driven', 'Beginner')

INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('3.00pm-4.00pm', 'Friday, 16 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Booked', 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('1.00pm-2.00pm', 'Saturday, 17 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Booked', 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('2.00pm-3.00pm', 'Monday, 19 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Booked', 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('3.00pm-4.00pm', 'Monday, 19 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Booked', 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('8.00am-9.00am', 'Tuesday, 20 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Booked', 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('9.00am-10.00am', 'Tuesday, 20 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Booked', 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('6.00pm-7.00pm', 'Wednesday, 21 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Booked', 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('7.00pm-8.00pm', 'Wednesday, 21 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Booked', 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('6.00pm-7.00pm', 'Thursday, 22 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Booked', 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('7.00pm-8.00pm', 'Thursday, 22 June 2017', 'Jeff Willow', 'Michael Reese', NULL, 'Booked', 'Beginner')

INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('8.00am-9.00am', 'Wednesday, 14 June 2017', 'Jeremy Bruin', 'Lee Kyle', NULL, 'Booked', 'Intermediate')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('9.00am-10.00am', 'Wednesday, 14 June 2017', 'Jeremy Bruin', 'Lee Kyle', NULL, 'Booked', 'Intermediate')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('8.00am-9.00am', 'Thursday, 15 June 2017', 'Jeremy Bruin', 'Lee Kyle', NULL, 'Booked', 'Intermediate')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('9.00am-10.00am', 'Thursday, 15 June 2017', 'Jeremy Bruin', 'Lee Kyle', NULL, 'Booked', 'Intermediate')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('8.00am-9.00am', 'Friday, 16 June 2017', 'Liam Watson', 'Lee Kyle', NULL, 'Booked', 'Intermediate')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('9.00am-10.00am', 'Friday, 16 June 2017', 'Liam Watson', 'Lee Kyle', NULL, 'Booked', 'Intermediate')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('10.00am-11.00am', 'Friday, 16 June 2017', 'Liam Watson', 'Lee Kyle', NULL, 'Booked', 'Intermediate')

INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('8.00am-9.00am', 'Thursday, 15 June 2017', 'Sarah Parker', 'Ben Bruin', NULL, 'Booked', 'Advanced')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('9.00am-10.00am', 'Thursday, 15 June 2017', 'Sarah Parker', 'Ben Bruin', NULL, 'Booked', 'Advanced')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('1.00pm-2.00pm', 'Friday, 16 June 2017', 'Sarah Parker', 'Ben Bruin', NULL, 'Booked', 'Advanced')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('2.00pm-3.00pm', 'Friday, 16 June 2017', 'Sarah Parker', 'Ben Bruin', NULL, 'Booked', 'Advanced')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('3.00pm-4.00pm', 'Friday, 16 June 2017', 'Liam Watson', 'Ben Bruin', NULL, 'Booked', 'Advanced')

/* Time off inserts */

INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('7.00am-8.00am', 'Friday, 23 June 2017', 'Liam Watson', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('8.00am-9.00am', 'Friday, 23 June 2017', 'Liam Watson', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('9.00am-10.00am', 'Friday, 23 June 2017', 'Liam Watson', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('10.00am-11.00am', 'Friday, 23 June 2017', 'Liam Watson', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('11.00am-12.00pm', 'Friday, 23 June 2017', 'Liam Watson', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('7.00am-8.00am', 'Saturday, 24 June 2017', 'Liam Watson', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('8.00am-9.00am', 'Saturday, 24 June 2017', 'Liam Watson', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('10.00am-11.00am', 'Saturday, 24 June 2017', 'Liam Watson', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('11.00am-12.00pm', 'Saturday, 24 June 2017', 'Liam Watson', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('12.00pm-1.00pm', 'Saturday, 24 June 2017', 'Liam Watson', NULL, 'Confirmed', NULL, NULL)

INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('7.00am-8.00am', 'Monday, 26 June 2017', 'Jeff Willow', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('8.00am-9.00am', 'Monday, 26 June 2017', 'Jeff Willow', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('9.00am-10.00am', 'Monday, 26 June 2017', 'Jeff Willow', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('10.00am-11.00am', 'Monday, 26 June 2017', 'Jeff Willow', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('11.00am-12.00pm', 'Monday, 26 June 2017', 'Jeff Willow', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('12.00pm-1.00pm', 'Monday, 26 June 2017', 'Jeff Willow', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('7.00am-8.00am', 'Tuesday, 27 June 2017', 'Jeff Willow', NULL, 'Declined', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('8.00am-9.00am', 'Tuesday, 27 June 2017', 'Jeff Willow', NULL, 'Declined', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('9.00am-10.00am', 'Tuesday, 27 June 2017', 'Jeff Willow', NULL, 'Declined', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('10.00am-11.00pm', 'Tuesday, 27 June 2017', 'Jeff Willow', NULL, 'Declined', NULL, NULL)

INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('7.00am-8.00am', 'Wednesday, 28 June 2017', 'Sarah Parker', NULL, 'Declined', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('8.00am-9.00am', 'Wednesday, 28 June 2017', 'Sarah Parker', NULL, 'Declined', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('9.00am-10.00am', 'Wednesday, 28 June 2017', 'Sarah Parker', NULL, 'Declined', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('10.00am-11.00pm', 'Wednesday, 28 June 2017', 'Sarah Parker', NULL, 'Declined', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('11.00am-12.00pm', 'Wednesday, 28 June 2017', 'Sarah Parker', NULL, 'Declined', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('12.00pm-1.00pm', 'Wednesday, 28 June 2017', 'Sarah Parker', NULL, 'Declined', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('7.00am-8.00am', 'Thursday, 29 June 2017', 'Sarah Parker', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('8.00am-9.00am', 'Thursday, 29 June 2017', 'Sarah Parker', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('9.00am-10.00am', 'Thursday, 29 June 2017', 'Sarah Parker', NULL, 'Confirmed', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('10.00am-11.00pm', 'Thursday, 29 June 2017', 'Sarah Parker', NULL, 'Confirmed', NULL, NULL)

INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('4.00pm-5.00pm', 'Monday, 26 June 2017', 'Jeremy Bruin', NULL, 'Requested', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('5.00pm-6.00pm', 'Monday, 26 June 2017', 'Jeremy Bruin', NULL, 'Requested', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('6.00pm-7.00pm', 'Monday, 26 June 2017', 'Jeremy Bruin', NULL, 'Requested', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('7.00pm-8.00pm', 'Monday, 26 June 2017', 'Jeremy Bruin', NULL, 'Requested', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('1.00pm-2.00pm', 'Tuesday, 27 June 2017', 'Jeremy Bruin', NULL, 'Requested', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('2.00pm-3.00pm', 'Tuesday, 27 June 2017', 'Jeremy Bruin', NULL, 'Requested', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('3.00pm-4.00pm', 'Tuesday, 27 June 2017', 'Jeremy Bruin', NULL, 'Requested', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('4.00pm-5.00pm', 'Tuesday, 27 June 2017', 'Jeremy Bruin', NULL, 'Requested', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('5.00pm-6.00pm', 'Tuesday, 27 June 2017', 'Jeremy Bruin', NULL, 'Requested', NULL, NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, TimeOff, Status, Type) VALUES ('6.00pm-7.00pm', 'Tuesday, 27 June 2017', 'Jeremy Bruin', NULL, 'Requested', NULL, NULL)



/*INSERT INTO Appointment VALUES ('New driver')
INSERT INTO Appointment VALUES ('Cornering')
INSERT INTO Appointment VALUES ('Parking')*/

/*INSERT INTO ClientType VALUES ('Beginner', '$200', 10)
INSERT INTO ClientType VALUES ('Intermediate','$150', 7)
INSERT INTO ClientType VALUES ('Advanced', '$100', 5)*/


--Drop statements
/*
DROP TABLE Admin
DROP TABLE ClientType
DROP TABLE Document
DROP TABLE Car
DROP TABLE Instructor
DROP TABLE Appointment
DROP TABLE Timeslot
DROP TABLE Client
DROP TABLE Books
DROP TABLE Receives
DROP TABLE Assigned
DROP TABLE Users
*/


--Select Statements
/*
Select * FROM Car
Select * FROM Admin
Select * FROM Instructor
Select * FROM Appointment
Select * FROM Timeslot
Select * FROM Client
Select * FROM Assigned
Select * FROM Users
*/